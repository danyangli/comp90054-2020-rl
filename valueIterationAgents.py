# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent
import collections


class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """

    def __init__(self, mdp, discount=0.9, iterations=100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter()  # A Counter is a dict with default 0
        self.runValueIteration()

    def runValueIteration(self):
        # Write value iteration code here
        "*** YOUR CODE HERE ***"

        i = self.iterations
        store_q = []

        while i > 0:
            i -= 1
            # print("~~~~~~~~~~~~i =", i)
            for state in self.mdp.getStates():
                if state is not 'TERMINAL_STATE':
                    maxQ = -10000000
                    for ac in self.mdp.getPossibleActions(state):
                        q = self.computeQValueFromValues(state, ac)
                        if q >= maxQ:
                            maxQ = q
                    store_q.append((state, maxQ))
                    # print((state, maxQ))

            for state, maxQ in store_q:
                    self.values[state] = maxQ

    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]

    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"

        q = 0
        if state is not 'TERMINAL_STATE':
            nextState = self.mdp.getTransitionStatesAndProbs(state, action)
            for ns in nextState:
                r = self.mdp.getReward(state, action, ns[0])
                q += ns[1] * (r + self.discount * self.getValue(ns[0]))
        return q
        util.raiseNotDefined()

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        maxQ = -1000000
        action = None
        for ac in self.mdp.getPossibleActions(state):
            q = self.computeQValueFromValues(state, ac)
            if q >= maxQ:
                maxQ = q
                action = ac
        return action
        util.raiseNotDefined()

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)


class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """

    def __init__(self, mdp, discount=0.9, iterations=1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"
        i = self.iterations
        states = self.mdp.getStates()
        index = 0

        while i > 0:
            i -= 1
            state = states[index]
            if state is not 'TERMINAL_STATE':
                maxQ = -10000000
                for ac in self.mdp.getPossibleActions(state):
                    q = self.computeQValueFromValues(state, ac)
                    if q >= maxQ:
                        maxQ = q
                self.values[state] = maxQ
            index += 1
            if index == len(states):
                index = 0

class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """

    def __init__(self, mdp, discount=0.9, iterations=100, theta=1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"

        pre = {}
        for state in self.mdp.getStates():
            predecessors = set()
            for pred in self.mdp.getStates():
                for ac in self.mdp.getPossibleActions(pred):
                    nextStates = self.mdp.getTransitionStatesAndProbs(pred, ac)
                    for ns in nextStates:
                        if ns[0] == state:
                            predecessors.add(pred)
            pre[state] = predecessors

        pq = util.PriorityQueue()
        for state in self.mdp.getStates():
            if not self.mdp.isTerminal(state):
                maxQ = -10000000
                for ac in self.mdp.getPossibleActions(state):
                    q = self.computeQValueFromValues(state, ac)
                    if q >= maxQ:
                        maxQ = q
                diff = abs(self.values[state] - maxQ)
                pq.push(state, -diff)

        i = self.iterations
        while i > 0:
            i -= 1
            if pq.isEmpty():
                break
            pop = pq.pop()
            if not self.mdp.isTerminal(pop):
                maxQ = -10000000
                for ac in self.mdp.getPossibleActions(pop):
                    q = self.computeQValueFromValues(pop, ac)
                    if q >= maxQ:
                        maxQ = q
                self.values[pop] = maxQ
            for p in pre[pop]:
                maxQ = -10000000
                for ac in self.mdp.getPossibleActions(p):
                    q = self.computeQValueFromValues(p, ac)
                    if q >= maxQ:
                        maxQ = q
                diff = abs(self.values[p] - maxQ)
                if diff > self.theta:
                    pq.update(p, -diff)


